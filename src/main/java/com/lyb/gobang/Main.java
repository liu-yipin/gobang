package com.lyb.gobang;

import com.lyb.gobang.frame.GobangFrame;

/**
 * 五子棋游戏 实现功能如下:
 *  1、有良好的UI界面，用户体验良好
 *  2、鼠标点击进行出棋子，玩家两人轮流下棋
 *  3、能够判断是否五子相连及输赢
 *  4、玩家能够重新开始游戏
 *  5、可以悔棋，要经过对方同意
 *  6、轮到玩家的回合可以认输，然后直接游戏结束
 *  7、如果棋盘看腻了，还可以切换棋盘
 *  8、在已经落有棋子的地方不能再落棋子
 *  9、有退出游戏的功能，并有用户手误操作的退出提示
 */

// 主函数入口
public class Main {
    public static void main(String[] args) {
        // 初始化一个五子棋界面对象
       GobangFrame gf = new GobangFrame();
       gf.initUI();
    }
}
