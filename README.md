# Gobang 单机版五子棋对战

### 一、实现功能
* 1、有良好的UI界面，用户体验良好
*  2、鼠标点击进行出棋子，玩家两人轮流下棋
*  3、能够判断是否五子相连及输赢
*  4、玩家能够重新开始游戏
*  5、可以悔棋，要经过对方同意
*  6、轮到玩家的回合可以认输，然后直接游戏结束
*  7、如果棋盘看腻了，还可以切换棋盘
*  8、在已经落有棋子的地方不能再落棋子
*  9、有退出游戏的功能，并有用户手误操作的退出提示

![在这里插入图片描述](https://img-blog.csdnimg.cn/da05bb1d55f943ed9cd0be138706a408.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_20,color_FFFFFF,t_70,g_se,x_16)

### 二、实现思路
##### 1、窗体设计
窗体实现    | 使用函数|参数描述
-------- |------ |-----|
设置标题| setTitle()|“五子棋”
设置图标|setIconImage()|(new ImageIcon("images/logo.png").getImage()) 图片
设置大小| SetSize()|（宽1200像素，高920像素）
设置显示位置| setLocation（）|（屏幕宽度-窗体宽度/2，屏幕高度-窗体高度/2）
设置大小是否可变| setResizable()|（false） 不能修改窗体大小
设置关闭方式| setDefaultCloseOperation（）|(JFrame.EXIT_ON_CLOSE) 关闭就退出程序
设置是否显示|setVisible()|(true)将窗体显示出来
添加鼠标监听|  addMouseListener()|（this) 表示给当前窗体添加监听
##### 2、棋盘设计
> 采用两种棋盘，一种需要直接绘制图片即可，另一种需要绘制图片、绘制棋盘格子和标注点位，两种棋盘都是18*18的格子。

```java
   /**
     *  棋盘
     *  1 [1号棋盘]
     *  2 [2号棋盘]
     */
    int checkerBoard = 2;
```
> 这里使用int型checkerBoard来记录当前是哪一个棋盘，若为1表示一号棋盘，若为2表示二号棋盘，这里用于切换棋盘的实现。

![在这里插入图片描述](https://img-blog.csdnimg.cn/40d1b562b715431f9265f5899ddc19a1.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_20,color_FFFFFF,t_70,g_se,x_16)


一号棋盘实现    | 使用函数|参数描述
-------- |------ |-----|
绘制图像| drawImage（）|（Image,x,y,observer）Image图片绘制在observer对象的x,y像素处
![在这里插入图片描述](https://img-blog.csdnimg.cn/dc5b2bee969046d09ea880d9971393a4.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_20,color_FFFFFF,t_70,g_se,x_16)




二号棋盘实现    | 使用函数|参数描述
-------- |------ |-----|
绘制图像| drawImage（）|（Image,x,y,observer）Image图片绘制在observer对象的x,y像素处
设置颜色| setColor（）|（Color.black）设置颜色为黑色
使用画线绘制格子|drawLine（）|(x1,y1,x2,y2)线的起始点坐标
使用画点绘制标志点位|fillOval()|（x,y,height,weight）点的坐标以及点的宽、高

##### 3、棋子坐标存储设计
```java
    /**
     * 保存棋子
     *  [0 ：无]
     *  [1 ：黑子]
     * [2 ： 白子]
     */
    int[][] allPiece = new int[19][19];

    // 记录上一个棋子位置
    int[] lastPiece = new int[2];
    // 下一步要下的是否是黑子
    boolean isBlack = true;
```
> 这里通过19*19的二维数组allPiece存放棋子的位置，默认为0，表示无棋子，1表示此处为黑子，2表示此处为白子，然后通过一维数组lastPiece来存放上一步棋子的位置，方便悔棋的实现，然后通过布尔型的isBlack来记录下一步是黑方下还是白方下，若为true表示下一步改黑方下，反之则是白方下。
##### 4、游戏状态设计
 ```java
   /**
     *  游戏状态 默认0
     *   [0：未开始]
     *   [1： 已开始]
     *   [2： 游戏结束]
     */
    int game_status = 0;
    // 提示信息
    String message = "黑方先行";
 ```
> 这里使用 int型game_status来记录游戏的状态，默认为0即是未开始游戏，1表示已开始游戏，2表示游戏结束。
> 右侧菜单和右侧上方的游戏信息都会根据当前游戏状态进行不同的绘制。

![在这里插入图片描述](https://img-blog.csdnimg.cn/db333ab59b32483691c1788527d9cab6.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_8,color_FFFFFF,t_70,g_se,x_16)




>提示信息会通过String类型的message记录，默认是黑方先行，若还没有开始游戏，这个也不会绘制上去，当开始游戏后，显示黑方先行，然后根据isBlack判断下次要执行的是黑方还是白方进行修改message的信息，即“轮到白方”和“轮到黑方”。

![在这里插入图片描述](https://img-blog.csdnimg.cn/b1bf10e17d5c4930a16f0fdcd219b667.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_20,color_FFFFFF,t_70,g_se,x_16)


![在这里插入图片描述](https://img-blog.csdnimg.cn/2e0052a2959d4d74b262e47be822c627.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_18,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/2df6222d158e43e0ba2c68128584d185.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_20,color_FFFFFF,t_70,g_se,x_16)
##### 5、开始游戏与重新开始游戏的实现

* ①棋盘数据清空，即将allPiece[i][j]置空
*  ②修改游戏状态，即game_status = 1，设置为开始游戏状态
*  ③游戏提示信息修改为初始状态，即message=“黑方先行”。
*  ④下一步棋改为黑方，即isBlack=true;

![在这里插入图片描述](https://img-blog.csdnimg.cn/5f6f8e3d024c4316b715c88b08bddb14.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_20,color_FFFFFF,t_70,g_se,x_16)


##### 6、悔棋的实现
*  ①上次落棋位置置空
*  ②调整下次落子颜色与上次一致
*  ③调整提示信息
* ④游戏状态设置为已开始
  ![在这里插入图片描述](https://img-blog.csdnimg.cn/f719e8e75dfb4c188745625c11d59c2c.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_20,color_FFFFFF,t_70,g_se,x_16)
> 如图所示，白方点击悔棋，白色棋子消失，提示信息改为"轮到白方"。

![在这里插入图片描述](https://img-blog.csdnimg.cn/b97712ec2ed34fee88d3422ea7ff596b.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_19,color_FFFFFF,t_70,g_se,x_16)

>再次落子也还是白子

![在这里插入图片描述](https://img-blog.csdnimg.cn/6d296ad70da144a080ed7eab97136cf9.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_20,color_FFFFFF,t_70,g_se,x_16)
##### 7、认输的实现
*  ①根据isBlack判断当前是谁的回合，判断是哪方认输
   *  ②设置游戏状态为结束状态
   ![在这里插入图片描述](https://img-blog.csdnimg.cn/e5fd82e05bac49dabe2d99ec2a513b2c.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_20,color_FFFFFF,t_70,g_se,x_16)
> 游戏结束，将无法再进行棋盘上的操作

![在这里插入图片描述](https://img-blog.csdnimg.cn/beb606abde5e4667b873a76b055671bf.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_20,color_FFFFFF,t_70,g_se,x_16)
##### 8、游戏结束的判断，即是否有同色五连棋子。
> 根据当前位置向四周进行判断，根据（横向、纵向、右上、左下）四个方向进行判断，使用count从1开始进行计数，同一个方向上每存在相连的同色棋子count就加1，每一个方向判断完了，就把count置为1，,直到出现count>=5则游戏胜利。
##### 9、双缓冲技术防止屏幕闪烁
```java
 BufferedImage bi = new BufferedImage(frame_width,frame_height,BufferedImage.TYPE_INT_ARGB);
 Graphics g2 = bi.createGraphics();
```




### 三、项目素材
##### 1、棋盘
![在这里插入图片描述](https://img-blog.csdnimg.cn/8d3e2482e6b34581bc4e564f0aa75094.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_20,color_FFFFFF,t_70,g_se,x_16)

![在这里插入图片描述](https://img-blog.csdnimg.cn/670a514bd23147e39a394c3677be0c5e.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_20,color_FFFFFF,t_70,g_se,x_16)
##### 2、菜单背景图
![在这里插入图片描述](https://img-blog.csdnimg.cn/8c4cafa9c74c4e5db75157ba798114ff.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_7,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/1958d80169d1458fa5ee148186bfee51.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA54ix5omT576955CD55qE56CB54y_,size_7,color_FFFFFF,t_70,g_se,x_16)
##### 3、黑白棋子
![在这里插入图片描述](https://img-blog.csdnimg.cn/99f1d5ce57d2466d9ede4e06879d2ab1.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/9dda6b621bb4498e9054edcfd0f46f91.png)
##### 4、游戏小图标
![在这里插入图片描述](https://img-blog.csdnimg.cn/ca595e19436947b6867d093c44290891.png)
